package org.voeetech.rxjava2.demo;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

import static io.reactivex.Flowable.fromIterable;
import static java.util.concurrent.TimeUnit.SECONDS;

public class ThrottlingOnEntry
{
    private static final Logger logger =
        LoggerFactory.getLogger(ThrottlingOnEntry.class);

    /**
     * This is a way of throttling requests to Iterator's calls to next(),
     * which frequency is now dictated by the interval() emitting items.
     */
    @Test
    public void throttleOnIterableNext()
    {
        Flowable<Long> timeKeeper = Flowable.interval(1, SECONDS)
                                            .onBackpressureDrop()
                                            .doOnNext(
                                                l -> logger.info("tick!"));

        Flowable<String> data = fromIterable(testIterable());

        Flowable.zip(data, timeKeeper, (d, t) -> d)
                .subscribeOn(Schedulers.io())
                .doOnNext(d -> logger.info("data! {}", d))
                .doOnComplete(() -> logger.info("done"))

                .blockingSubscribe();

    }

    private Iterable<String> testIterable()
    {

        return () -> new Iterator<String>()
        {
            int i = 10;

            @Override
            public boolean hasNext()
            {
                if (--i > 0)
                    return true;
                return false;
            }

            @Override
            public String next()
            {
                logger.info("iterator next() called");
                return "ITEM_" + i;
            }
        };
    }
}
