package org.voeetech.rxjava2.demo;

import com.google.common.base.Stopwatch;
import io.reactivex.Flowable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static org.voeetech.rxjava2.demo.support.DemoSupport.pages;
import static org.voeetech.rxjava2.demo.support.DemoSupport.throwExceptionOnNthItem;

public class Simple
{
    private static final Logger logger = LoggerFactory.getLogger(Simple.class);

    private Stopwatch sw;

    @Before
    public void init() { sw = Stopwatch.createStarted(); }

    @After
    public void tearDown()
    {
        logger.info("Completed in {}ms.",
                    sw.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * No subscription, no fun! No items are emitted from anywhere.
     */
    @Test
    public void simpleFlowWithNoSubscription()
    {

        Flowable.fromIterable(pages(2, 2))
                .flatMap(Flowable::fromIterable)
                .doOnNext(s -> logger.info("subscriber: onNext() -> {}", s))
                .doOnError(e -> logger.error("subscriber: onError()", e))
                .doOnComplete(() -> logger.info("subscriber: onComplete()"));

    }

    /**
     * After subscribing, the items start flowing through the stream,
     * ending up in the Subscriber which is the data sink.
     * <p>
     * Iterable(pageIterator) -> Iterable(List) -> Subscriber
     */
    @Test
    public void simpleFlowWithSubscription()
    {

        Flowable.fromIterable(pages(2, 2))
                .flatMap(Flowable::fromIterable)
                .subscribe(s -> logger.info("subscriber: onNext() -> {}.", s),
                           t -> logger.error("subscriber: onError()", t),
                           () -> logger.info("subscriber: onComplete()")
                );

    }

    /**
     * Detailing all emissions on various steps.
     */
    @Test
    public void simpleFlowWithSubscriptionDetailed()
    {

        Flowable.fromIterable(pages(2, 2))
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .flatMap(Flowable::fromIterable)
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .subscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );

    }

    /**
     * When en error happens in the first observable, the stream is 'broken'
     * and the subscriber's onError() is called
     */
    @Test
    public void simpleFlowWithError()
    {

        Flowable.fromIterable(throwExceptionOnNthItem(pages(2, 2),
                                                      2))
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnError(t -> logger.info("fromIterable: onError()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .flatMap(Flowable::fromIterable)
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnError(t -> logger.info("\tflatMap: onError()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .subscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );

    }

    /**
     * An error can happen further down the pipeline with the same result: the
     * error is propagated down the stream.
     * <p>
     * The 3rd page will never be emitted from the top observer
     */
    @Test
    public void simpleFlowWithError2()
    {

        Flowable.fromIterable(pages(10, 2))
                .map(l -> throwExceptionOnNthItem(l, 2))
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnError(t -> logger.info("fromIterable: onError()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .flatMap(l -> Flowable.fromIterable(l)
                                      .hide()) //hide() is here because of bug in rxJava2
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnError(t -> logger.info("\tflatMap: onError()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .subscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );

    }

}
