package org.voeetech.rxjava2.demo;

import com.google.common.base.Stopwatch;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.voeetech.rxjava2.demo.support.DemoSupport.delay;
import static org.voeetech.rxjava2.demo.support.DemoSupport.pages;
import static org.voeetech.rxjava2.demo.support.DemoSupport.throwExceptionOnNthItem;

public class Parallel
{
    private static final Logger logger =
        LoggerFactory.getLogger(Parallel.class);

    private Stopwatch sw;

    @Before
    public void init() { sw = Stopwatch.createStarted(); }

    @After
    public void tearDown()
    {
        logger.info("Completed in {}ms.",
                    sw.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * Simulating slow IO calls.
     * A second delay per page for the top observer
     * A second delay per item for the iterator
     * 4 * 1s + 5 * 1s = 24s
     * <p>
     * Sequential IS the default behaviour
     */
    @Test
    public void sequentialFlow()
    {
        Flowable.fromIterable(delay(pages(4, 5), 1000))
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .map(l -> delay(l, 1000))
                .flatMap(Flowable::fromIterable)
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .subscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

    /**
     * The same flow running parts of the stream in parallel.
     * <p>
     * 4 * 1s + 5 * 1s = 9s
     * <p>
     * That's great, but the real killer feature is the thread coordination:
     * the flatMap() will only emit onComplete() when
     * 1. The producer upstream has sent onComplete()
     * AND
     * 2. All the subflows created within the flatMap() have sent their
     * onComplete().
     */

    @Test
    public void parallelFlow()
    {
        Flowable.fromIterable(delay(pages(4, 5), 1000))
                .subscribeOn(Schedulers.io())
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .map(l -> delay(l, 1000))
                .flatMap(
                    l -> Flowable.fromIterable(l)
                                 .subscribeOn(Schedulers.io())
                )
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

    /**
     * The flow reacts to the errors in the same way as in the sequential
     * example. If an error signal flows into flatMap it will cancel all existing
     * internal subscriptions and forward the error to the downstream
     */

    @Test
    public void parallelFlowError()
    {
        Flowable
            .fromIterable(throwExceptionOnNthItem(delay(pages(4, 10), 3000), 3))
            .subscribeOn(Schedulers.io())
            .doOnNext(l -> logger.info("fromIterable: onNext()"))
            .doOnError(t -> logger.info("fromIterable: onError()"))
            .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
            .map(l -> delay(l, 400))
            .flatMap(
                l -> Flowable.fromIterable(l)
                             .subscribeOn(Schedulers.io()))
            .doOnNext(s -> logger.info("\tflatMap: onNext()"))
            .doOnError(t -> logger.info("\tflatMap: onError()"))
            .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
            .blockingSubscribe(
                s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                t -> logger.error("\t\tsubscriber: onError()", t),
                () -> logger.info("\t\tsubscriber: onComplete()")
            );
    }

    /**
     * The flow reacts to the errors in flatMap's subflows in the same way as in the sequential
     * example even though the error happens on some separate thread.
     * This is powerful, it means that flatMap acts as a sort of mediator
     * even in asynchronous scenario. If any internal flow emits error, flatMap, will
     * cancel all existing subscriptions and emit the error out to downstream
     */

    @Test
    public void parallelFlowErrorFromWithinFlatMap()
    {
        //the second list coming from the top 'fromIterable'
        //will be armed with exception thrown on iteration...
        final AtomicInteger counter = new AtomicInteger(2);

        Flowable.fromIterable(delay(pages(4, 10), 3000))
                .subscribeOn(Schedulers.io())
                .doOnNext(l -> logger.info("fromIterable: onNext()"))
                .doOnError(t -> logger.info("fromIterable: onError()"))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .map(l -> delay(l, 800))
                .map(l -> {
                    if(counter.decrementAndGet() == 0)
                        //... while trying to get the 10th item
                        return throwExceptionOnNthItem(l, 10);
                    else
                        return l;

                })
                .flatMap(
                    l -> Flowable.fromIterable(l)
                                 .subscribeOn(Schedulers.io())
                )
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnError(t -> logger.info("\tflatMap: onError()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }
}
