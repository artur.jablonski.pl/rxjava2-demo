package org.voeetech.rxjava2.demo.support;

import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.stream.Collectors.toList;

public class DemoSupport
{
    /**
     * Generates a list of 'pages'. Each page being a list of String objects
     * int the format ITEM_&lt;pageNumber&gt;_&lt;itemNumber&gt;
     * <p>
     * The intention is to simulate a paged retreival of data
     *
     * @param numberOfPages number of pages to generate
     * @param pageSize      size of each page
     * @return
     */
    public static List<List<String>> pages(final int numberOfPages,
                                           final int pageSize)
    {
        return
            IntStream.range(0, numberOfPages)
                     .mapToObj(
                         p -> IntStream.range(0, pageSize)
                                       .mapToObj(i -> "ITEM_page" + p + "_#" + i)
                                       .collect(toList())
                     )
                     .collect(toList());
    }

    /**
     * Takes a list and returns it with an iterator that adds a delay when
     * next() is called.
     * This is to simulate slow network calls
     *
     * @param target
     * @param delayMillis
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> delay(List<T> target,
                                    int delayMillis)
    {
        return
            (List) Proxy.newProxyInstance(
                DemoSupport.class.getClassLoader(),
                new Class[] { List.class },
                (p, m, args) -> {
                    if (m.getName().equals("iterator")) {
                        return toIteratorWithDelay(target.iterator(),
                                                   delayMillis);
                    }
                    return m.invoke(target, args);
                });
    }

    /**
     * Takes a list and returns it with an iterator that throws an exception
     * on the nth call to next()
     * This is to simulate an exception happening during data processing
     *
     * @param target
     * @param n
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> throwExceptionOnNthItem(List<T> target,
                                                      int n)
    {
        return
            (List) Proxy.newProxyInstance(
                DemoSupport.class.getClassLoader(),
                new Class[] { List.class },
                (p, m, args) -> {
                    if (m.getName().equals("iterator")) {
                        return toIteratorThatThrowsExceptionOnNthItem(
                            target.iterator(), n);
                    }
                    return m.invoke(target, args);
                });
    }

    @SuppressWarnings("unchecked")
    private static <T> Iterator<T> toIteratorWithDelay(Iterator<T> target,
                                                       int delayMillis)
    {
        return
            (Iterator) Proxy.newProxyInstance(
                DemoSupport.class.getClassLoader(),
                new Class[] { Iterator.class },
                (p, m, args) -> {
                    if (m.getName().equals("next")) {
                        sleepUninterruptibly(delayMillis,
                                             TimeUnit.MILLISECONDS);
                    }
                    return m.invoke(target, args);
                });
    }

    @SuppressWarnings("unchecked")
    private static <T> Iterator<T> toIteratorThatThrowsExceptionOnNthItem(
        Iterator<T> target, int n)
    {
        AtomicInteger counter = new AtomicInteger(0);
        return
            (Iterator) Proxy.newProxyInstance(
                DemoSupport.class.getClassLoader(),
                new Class[] { Iterator.class },
                (p, m, args) -> {
                    if (m.getName().equals("next")) {
                        if (counter.incrementAndGet() == n)
                            throw new RuntimeException("Kaboom!");
                    }
                    return m.invoke(target, args);
                });
    }

}
