package org.voeetech.rxjava2.demo;

import com.google.common.base.Stopwatch;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.voeetech.rxjava2.demo.support.DemoSupport.pages;

public class PublishZip
{
    private static final Logger logger =
        LoggerFactory.getLogger(PublishZip.class);

    private Stopwatch sw;

    @Before
    public void init() { sw = Stopwatch.createStarted(); }

    @After
    public void tearDown()
    {
        logger.info("Completed in {}ms.",
                    sw.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * Here I am multicasting the generated list to two destinations
     * One will calculate the size of the list and the other double size
     * of the list, then zip will zip both back to which it a subscriber subscribes
     * <p>
     * Everything is sequential.
     */

    @Test
    public void publishZip()
    {
        Flowable<List<String>> top =
            Flowable.fromIterable(pages(2, 3))
                    .doOnNext(l -> logger.info(
                        "fromIterable: onNext()"))
                    .doOnComplete(
                        () -> logger.info("fromIterable: onComplete() "))
                    .publish()
                    .autoConnect(2)
                    .doOnNext(l -> logger.info(
                        "publish: onNext()"))
                    .doOnComplete(() -> logger.info("publish: onComplete() "));

        Flowable<Integer> size =
            top.map(l -> l.size())
               .doOnNext(l -> logger.info(
                   "size: onNext()"))
               .doOnComplete(() -> logger.info("size: onComplete() "));

        Flowable<Integer> doubleSize =
            top.map(l -> 2 * l.size())
               .doOnNext(l -> logger.info(
                   "doubleSize: onNext()"))
               .doOnComplete(() -> logger.info("doubleSize: onComplete() "));

        Flowable.zip(size, doubleSize, (s, d) -> s + " " + d)
                .doOnNext(l -> logger.info(
                    "zip: onNext()"))
                .doOnComplete(() -> logger.info("zip: onComplete() "))

                .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

    /**
     * same thing, this time with concurrency
     */
    @Test
    public void publishZipConcurrent()
    {
        Flowable<List<String>> top =
            Flowable.fromIterable(pages(2, 3))
                    .doOnNext(l -> logger.info(
                        "fromIterable: onNext()"))
                    .doOnComplete(
                        () -> logger.info("fromIterable: onComplete() "))
                    .publish()
                    .autoConnect(2)
                    .doOnNext(l -> logger.info(
                        "publish: onNext()"))
                    .doOnComplete(() -> logger.info("publish: onComplete() "));

        Flowable<Integer> size =
            top.observeOn(Schedulers.io())
               .map(l -> l.size())
               .doOnNext(l -> logger.info(
                   "size: onNext()"))
               .doOnComplete(() -> logger.info("size: onComplete() "));

        Flowable<Integer> doubleSize =
            top.observeOn(Schedulers.io())
               .map(l -> 2 * l.size())
               .doOnNext(l -> logger.info(
                   "doubleSize: onNext()"))
               .doOnComplete(() -> logger.info("doubleSize: onComplete() "));

        Flowable.zip(size, doubleSize, (s, d) -> s + " " + d)
                .doOnNext(l -> logger.info(
                    "zip: onNext()"))
                .doOnComplete(() -> logger.info("zip: onComplete() "))

                .subscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

}
