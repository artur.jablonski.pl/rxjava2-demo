package org.voeetech.rxjava2.demo;

import com.google.common.base.Stopwatch;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.voeetech.rxjava2.demo.support.DemoSupport.delay;
import static org.voeetech.rxjava2.demo.support.DemoSupport.pages;

public class Backpressure
{
    private static final Logger logger =
        LoggerFactory.getLogger(Backpressure.class);

    private Stopwatch sw;

    @Before
    public void init() { sw = Stopwatch.createStarted(); }

    @After
    public void tearDown()
    {
        logger.info("Completed in {}ms.",
                    sw.stop().elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * Observable doesn't do backpressure
     */

    @Test
    public void ObservableInParallelFlow()
    {
        AtomicInteger counter = new AtomicInteger(0);

        Observable.fromIterable(pages(1000, 5))
                  .subscribeOn(Schedulers.io())
                  .doOnNext(l -> logger.info(
                    "fromIterable: onNext() #" + counter.incrementAndGet()))
                  .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                  .map(l -> delay(l, 1000))
                  .flatMap(
                    l -> Observable.fromIterable(l)
                                 .subscribeOn(Schedulers.io())
                )
                  .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                  .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                  .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

    /**
     * Backpressure, THE solution for the fast producer -> slow consumer
     * problem in async computing
     *
     * Downstream can signal to upstream to slow down!
     *
     * by default the backpressure buffer is 128
     */

    @Test
    public void backpressureInParallelFlow()
    {
        AtomicInteger counter = new AtomicInteger(0);

        Flowable.fromIterable(pages(150, 5))
                .subscribeOn(Schedulers.io())
                .doOnNext(l -> logger.info(
                    "fromIterable: onNext() #" + counter.incrementAndGet()))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .map(l -> delay(l, 1000))
                .flatMap(
                    l -> Flowable.fromIterable(l)
                                 .subscribeOn(Schedulers.io())
                )
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }

    /**
     * The buffer can be changed for each element in stream (that supports it)
     */

    @Test
    public void backpressure500BufferInParallelFlow()
    {
        AtomicInteger counter = new AtomicInteger(0);

        Flowable.fromIterable(pages(550, 5))
                .subscribeOn(Schedulers.io())
                .doOnNext(l -> logger.info(
                    "fromIterable: onNext() #" + counter.incrementAndGet()))
                .doOnComplete(() -> logger.info("fromIterable: onComplete() "))
                .map(l -> delay(l, 100))
                .flatMap(
                    l -> Flowable.fromIterable(l)
                                 .subscribeOn(Schedulers.io()),
                    false,
                    500,
                    500
                )
                .doOnNext(s -> logger.info("\tflatMap: onNext()"))
                .doOnComplete(() -> logger.info("\tflatMap: onComplete()"))
                .blockingSubscribe(
                    s -> logger.info("\t\tsubscriber: onNext() -> {}.", s),
                    t -> logger.error("\t\tsubscriber: onError()", t),
                    () -> logger.info("\t\tsubscriber: onComplete()")
                );
    }
}
