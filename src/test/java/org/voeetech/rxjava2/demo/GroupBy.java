package org.voeetech.rxjava2.demo;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GroupBy
{
    private static final Logger logger = LoggerFactory.getLogger(GroupBy.class);

    /**
     * The scenario is that you have a stream where various items appear,
     * you want to group those according to some function and process items
     * in the same group sequentially while those being in different groups
     * can be processed in parallel
     * <p>
     * This is how to do this using groupBy
     */
    @Test
    public void sequentialWithinGroup()
    {

        Flowable.fromIterable(getRandomData(3))
                .groupBy(d -> d.key)
                .flatMap(f -> f.subscribeOn(Schedulers.computation())
                               .doOnNext(i -> logger
                                   .info("key: {}, value: {}", i.key, i.value)))
                .blockingSubscribe();

    }

    private List<Data> getRandomData(int numOfGroups)
    {
        List<Data> result = new ArrayList<>();

        Random r = new Random();
        int counter = 0;

        for (int i = 0; i < 100; i++) {
            int key = r.nextInt(numOfGroups);
            result.add(new Data(key, i));
        }

        return result;
    }

    private class Data
    {
        private Integer key;
        private Integer value;

        public Data(Integer key, Integer value)
        {
            this.key = key;
            this.value = value;
        }
    }

}


