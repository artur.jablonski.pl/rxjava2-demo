package org.voeetech.rxjava2.demo;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class SharedStreamWebFlux
{
    private static final Logger logger =
        LoggerFactory.getLogger(SharedStreamWebFlux.class);

    private static class Request
    {
        public Integer id;
        public String user;

        public Request withId(Integer id)
        {
            this.id = id;
            return this;
        }

        public Request withUser(String user)
        {
            this.user = user;
            return this;
        }

    }

    private static class Response
    {
        public Integer id;
        public String user;

        public Response withId(Integer id)
        {
            this.id = id;
            return this;
        }

        public Response withUser(String user)
        {
            this.user = user;
            return this;
        }
    }

    private Flowable<Response> shared;
    private AtomicInteger counter = new AtomicInteger();
    private FlowableEmitter<Request> emitter;

    @Before
    public void init()
    {

        shared =
            Flowable.<Request>create(
                s -> this.emitter = s,
                BackpressureStrategy.BUFFER
                //todo this is the weak point, there is no backpressure
                //so unbounded buffering is used for requests, some fail fast would be better
                //todo perhaps Flowable.generate with some managed queue would do the trick
                //also, how do I signal error for one user without blowing up the whole shared stream for everyone?
            ).groupBy(r -> r.user)
             .flatMap(gf ->
                          gf.concatMap(
                              //this is the synchronized code for one user
                              r -> Flowable.just(r)
                                           .doOnNext(
                                               r1 -> logger
                                                   .info(
                                                       "Request in for id: [{}:{}]",
                                                       r1.user,
                                                       r1.id))
                                           .delay(1, TimeUnit.SECONDS)
                                           .map(r1 -> new Response()
                                               .withId(r1.id)
                                               .withUser(
                                                   r1.user))
                          )
             )
             .share();

        shared.subscribe();

    }

    //this emulates controller
    private Single<Response> doRequest(String user)
    {
        Integer id = counter.getAndIncrement();
        return
            shared
                .doOnSubscribe(s -> emitter
                    .onNext(new Request().withId(id).withUser(user)))
                .filter(r -> r.id.equals(id))
                .firstOrError()
                .subscribeOn(Schedulers.io())
                .doOnSuccess(r -> logger
                    .info("Response out for user: [{}:{}]", r.user, r.id));
    }

    @Test
    //this emulates requests made to controller and controller subscribing
    public void simulateRequestsToController()
    {
        Flowable.fromArray("user1",
                           "user2",
                           "user1",
                           "user1",
                           "user1",
                           "user2",
                           "user2",
                           "user2",
                           "user1",
                           "user1")
                .flatMapSingle(this::doRequest, false, 10)
                .ignoreElements()
                .blockingAwait();

    }
}
