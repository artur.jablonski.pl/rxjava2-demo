package org.voeetech.rxjava2.demo;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class FlatMapInDepth
{
    private static final Logger logger =
        LoggerFactory.getLogger(FlatMapInDepth.class);

    /**
     * Basic scenario for the examples,
     * Generating 10 integers and mapping them to a double
     */
    @Test
    public void base()
    {
        Flowable.range(0, 10)
                .map(i -> 2 * i)
                .doOnNext(i -> logger.info("{}", i))
                .blockingSubscribe();
    }

    /**
     * One way of thinking about flatmap is that it allows to generate
     * 0..n signals for every signal flowing into it
     * This is unlike map() where there's always 1 -> 1 transformation
     */
    @Test
    public void swallowingOrMultiplyingSignals()
    {
        Flowable.range(0, 10)
                .flatMap(i -> {
                             switch (i) {
                             case 0:
                                 return Flowable.just(i, i);
                             case 5:
                                 return Flowable.empty();
                             default:
                                 return Flowable.just(i, i, i);
                             }
                         }
                )
                .doOnNext(i -> logger.info("{}", i))
                .blockingSubscribe();
    }


    /**
     * Heavy computation over available cores
     */
    @Test
    public void flatMapForParallellProcessing()
    {
        Flowable.range(0, 10)
                .doOnSubscribe(__ -> logger.info("start"))
                .flatMap(i ->
                             Flowable.fromCallable(
                                 () -> {
                                     sleep(1000);
                                     return i * 2;
                                 }
                             )
                                     .subscribeOn(Schedulers.computation())
                )
                .doOnNext(i -> logger.info("{}", i))
                .doOnComplete(() -> logger.info("done"))
                .blockingSubscribe();

    }

    /**
     * IO calls
     */
    @Test
    public void flatMapForIOParallellProcessing()
    {
        Flowable.range(0, 100)
                .flatMap(i ->
                             Flowable.timer(1, TimeUnit.SECONDS)
                                     .map(__ -> i * 2)
                                     .subscribeOn(Schedulers.io())
                )
                .doOnNext(i -> logger.info("{}", i))
                .blockingSubscribe();

    }
}


